import React from 'react'
import Card from '../components/Card'
import ServiceCard from '../components/ServiceCard'
import Footer from '../components/Footer'

const Home = () => {
  return (
  <div className='min-h-screen'>
    <section id='home' className='px-14'>
        <div className='flex justify-between w-full h-screen items-center'>

            {/* LEFT SIDE CONTENT OF HOME */} 
            <div className='flex flex-col w-full md:w-[50%] gap-3'>
                <div className='w-full flex h-fit'>
                    <div className={`w-full rounded-xl shrink-0`}>
                        <div className='flex gap-3 w-[100%] shrink-0'>
                        <div className={` w-full items-start justify-start p-4`}>
                            <div className=''><h1 className='font-bold text-2xl'>Best Paediatric Surgeon in Nepal</h1></div>
                            <div className=''>
                                <p className='py-3'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eveniet assumenda hic soluta explicabo iste. 
                                    Nihil odit tempora soluta eligendi placeat error praesentium nobis aperiam vel. Porro qui vero inventore obcaecati.</p>
                                <p className='py-3'>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ipsam accusantium voluptatem expedita? Repudiandae 
                                    voluptatibus, veritatis vel praesentium dicta qui aliquam doloribus repellendus odit saepe officiis. Labore, accusantium hic. Quibusdam, nemo.</p>
                            </div>
                            <div>
                                <button className='px-4 py-1 border-2 border-slate-600  shadow-[#ffffff] shadow-sm hover:shadow-md hover:shadow-[#ffffff] rounded-2xl text-[#232323]'>BOOK AN APPOINTMENT</button>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* RIGHT SIDE CONTENT OF HOME */}
            <div className='w-[50%] text-center flex justify-center'>
                <div className='flex justify-center w-full'>
                <div className='rounded-full h-64 w-64 bg-slate-500 flex justify-center relative'>
                    <img src='https://www.infodoctor.in/assets/frontend/images/banner.png' className='absolute bottom-0 rounded-b-full'/>
                    <div className='p-2 bg-[#fefdfd] shadow-xl shadow-[#717171] rounded-md absolute bottom-0 left-0 flex text-left -translate-x-14 -translate-y-6'>
                        <div className='flex items-center justify-center p-1 mr-2 text-slate-600'><i className="fa-solid fa-circle-h text-2xl"></i></div>
                        <div className='text-[12px]'>
                            <p>Dr.Roshan Kumar Shah</p>
                            <p>MBBS,MS(Pediatric Surgeon)</p>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>

    <section id='info' className='bg-slate-100 px-14 min-h-48 py-4 mb-8'>
        <div className='w-full'>
            <h2 className='w-full flex justify-center font-bold text-[#232323] text-2xl'>Our Notable Partners</h2>
            {/* <div className='w-full flex justify-center'> */}
                <div className='pt-8 grid grid-cols-4 w-full place-items-center'>
                    <Card image='https://avisbudget.wd1.myworkdayjobs.com/Zipcar_Careers/assets/logo'/>
                    <Card image='https://avisbudget.wd1.myworkdayjobs.com/Zipcar_Careers/assets/logo'/>
                    <Card image='https://avisbudget.wd1.myworkdayjobs.com/Zipcar_Careers/assets/logo'/>
                    <Card image='https://static1.squarespace.com/static/5a1f012fc027d8b65276b7e4/t/5a1f1cf5f9619ad38acd79c9/1511988481093/216549---Everest---Logo---Hospice.png' />
                </div>
            {/* </div> */}
        </div>
    </section>

    <section id='service' className=' px-14 min-h-48 mb-7 '>
        <div className='w-full py-4'>
        <h2 className='w-full flex justify-center font-bold text-[#232323] text-2xl'>Services</h2>
            <div className='pt-8 grid grid-cols-4 w-full place-items-center gap-3'>
                <ServiceCard image='https://th.bing.com/th/id/OIP.UQBuhcBUPtz0ZxQyFvV4pwHaBW?rs=1&pid=ImgDetMain' />
                <ServiceCard image='https://th.bing.com/th/id/OIP.UQBuhcBUPtz0ZxQyFvV4pwHaBW?rs=1&pid=ImgDetMain' />
                <ServiceCard image='https://th.bing.com/th/id/OIP.UQBuhcBUPtz0ZxQyFvV4pwHaBW?rs=1&pid=ImgDetMain' />
                <ServiceCard image='https://th.bing.com/th/id/OIP.UQBuhcBUPtz0ZxQyFvV4pwHaBW?rs=1&pid=ImgDetMain' />
                <ServiceCard image='https://th.bing.com/th/id/OIP.UQBuhcBUPtz0ZxQyFvV4pwHaBW?rs=1&pid=ImgDetMain' />
                <ServiceCard image='https://th.bing.com/th/id/R.86b3fa344f96c52172e9db8b13245972?rik=%2blfaZ9lofnUIEQ&riu=http%3a%2f%2fsimplyknowledge.com%2fuploads%2fpage%2fheadimg_url%2f211%2fLogo.png&ehk=D5YQZEyuwzTHoYg2lRH6mc3Y9G3HXj1zX05rt49E2aQ%3d&risl=&pid=ImgRaw&r=0' />
            </div>
        </div>
    </section>

    <section id='gallery' className='mb-8 px-10'>
        <div>
            <h2 className='w-full flex justify-center font-bold text-[#232323] text-2xl'>Gallery</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Accusantium quis tempore itaque corrupti magni aut 
                dolorem blanditiis? Doloribus numquam minus ab culpa corrupti magni repellendus amet consectetur nostrum, sint corporis.</p>
        </div>
    </section>

    <section id='contact-us' className='mb-8 bg-slate-100 px-32 min-h-32 py-4'>
        <div>
            <h2 className='w-full flex justify-center font-bold text-[#232323] text-2xl'>Contact Us</h2>
            <p className='text-center text-[#767676]'>Let us know your problem.</p>
            <form>
            <div className='flex justify-between mt-6 text-[#717171]'>
                <div className='relative p-1'>
                    <i className="fa-solid fa-user absolute left-2 top-4"></i>
                    <input type="text" id="name" placeholder='FullName' className='p-2 pl-6 rounded-md focus:outline-none' required/>
                </div>

                <div className='relative p-1'>
                    <i className="fa-solid fa-envelope absolute left-2 top-4"></i>
                    <input type="email" id="email" placeholder='Email' className='p-2 pl-6 rounded-md focus:outline-none' required/>
                </div>
                
                <div className='relative p-1'>
                    <i className="fa-solid fa-phone-flip absolute left-2 top-4"></i>
                    <input type="tel" id="phone" placeholder='Phone Number' className='p-2 pl-6 rounded-md focus:outline-none' required/>
                </div>
            </div>
            <div className='w-full mt-6 relative group text-[#717171]'>
                <i className="fa-solid fa-comment absolute top-3 left-1 group-focus:hidden"></i>
                <textarea className='w-full p-2 pl-5 rounded-md focus:outline-none' placeholder='Your Message'></textarea>
            </div>

            <button type='submit' className='mt-6 px-4 py-1 border-2 border-slate-600 bg-slate-600 shadow-[#ffffff] shadow-sm hover:shadow-md hover:shadow-[#ffffff] rounded-lg text-white'>Send Message</button>
            </form>
        </div>
    </section> 

    <Footer/>
  </div>
  )
}

export default Home
