import React from 'react'

const ServiceCard = (props) => {
  return (
    <div className="max-w-sm rounded overflow-hidden shadow-lg h-36 px-6 py-2">
        <div className='w-[30%]'>
        <img className="bg-cover" src={props.image} alt='services' />
        </div>
      <div className="">
        <div className="font-bold text-xl mb-2">Piles</div>
        <p className="text-gray-700 text-base">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </p>
      </div>
    </div>
      
  )
}

export default ServiceCard
