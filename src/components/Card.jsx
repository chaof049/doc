import React from 'react'

const Card = (props) => {
  return (
    <div className='w-32'>
      <img className='bg-contain' src={props.image} />
    </div>
  )
}

export default Card
