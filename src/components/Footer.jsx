import React from 'react'

const Footer = () => {

  return (
    <div className='flex px-10 py-8 text-[#232323]'>
        <div className='flex-1'>
            <table className='text-left'>
                <tr className='py-1'>
                    <th>Services</th>
                    <th>Company</th>
                    <th>Helpful Link</th>
                </tr>
                <tr className='py-2'>
                    <td className='pr-4'>Consultations</td>
                    <td className='pr-4'>Consultations</td>
                    <td className='pr-4'>Consultations</td>
                </tr>
                <tr className='py-2'>
                    <td className='pr-4'>Consultations</td>
                    <td className='pr-4'>Consultations</td>
                    <td className='pr-4'>Consultations</td>
                </tr>
                <tr className='py-2'>
                    <td className='pr-4'>Consultations</td>
                    <td className='pr-4'>Consultations</td>
                    <td className='pr-4'>Consultations</td>
                </tr>
                <tr className='py-2'>Recovery</tr>
                <tr className='py-2'>Pain Management</tr>
            </table>

            <div className='py-3  text-[#808080] text-sm'>
                <hr></hr>
                <div className='flex gap-4'>
                    <p>Terms and Conditions</p>
                    <p>Privacy Policy</p>
                    <p>Cookies</p>
                </div>
                <div>@2022.Company Name. All rights reserved.</div>
            </div>
        </div>

        <div className='flex-1 text-right pl-5'>
            <p className='underline text-[#808080]'>WE ARE HERE TO HELP</p>
            <div><h2 className='text-2xl font-semibold'>Pediatric Surgeon Nepal</h2></div>
            <p className='py-4'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto eligendi dolores quibusdam perferendis maxime nesciunt 
                dicta rem in neque, commodi ut consequuntur asperiores unde illo aliquid voluptatibus molestias sit amet.</p>
            <div>
                <button className='px-4 py-1 border-2 border-slate-600  shadow-[#ffffff] shadow-sm hover:shadow-md hover:shadow-[#ffffff] rounded-2xl text-[#232323]'>BOOK AN APPOINTMENT</button>
            </div>
        </div>
      
    </div>
  )
}

export default Footer
