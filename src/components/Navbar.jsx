// import { Link} from 'react-router-dom'
import {HashLink as Link} from "react-router-hash-link"
import { useLocation } from 'react-router-dom';

const Navbar = () => {
  const location = useLocation();
  const data = false
  const theme = true

  return (
  <div className={`fixed top-0 left-0 z-[99] w-full bg-white`}>
    <div className={`flex px-3 py-3 justify-between sm:px-14 sm:py-5 items-center`}>
        <div className="flex gap-6 items-center">
          <div className="flex items-center text-slate-600">
            <i className="fa-solid fa-hospital-user bg-transparent  text-[18px] mr-2"></i>
            <div className={`font-bold`}>Paediatric Surgeon Nepal</div>
          </div>
        </div>
        {!data?
        <div className='flex'>
        <Link smooth to='/#home'>
          <button className={` items-center pr-3 pl-1 mr-4 border-r-[3px] rounded-[4px] ${(location.pathname=='/')?'border-slate-600 text-slate-600':'border-[#dfdede] text-[#555555]'}  hover:border-slate-600 hover:text-slate-600 hover:shadow-sm hover:shadow-slate-600`}>
            <span className={`hidden sm:block`}>Home</span>
          </button>
          </Link>
          <Link smooth to='/#info'>
          <button className={`items-center pr-3 pl-1 mr-4 border-r-[3px] rounded-[4px] ${(location.pathname=='/info')?'border-slate-600 text-slate-600':'border-[#dfdede] text-[#555555]'}  hover:border-slate-600 hover:text-slate-600 hover:shadow-sm hover:shadow-slate-600`}>
            <span className={` hidden sm:block`}>Information</span>
          </button>
          </Link>
          <Link smooth to='/#service'>
          <button className={`flex items-center pr-3 pl-1 mr-4 border-r-[3px] rounded-[4px] ${(location.pathname=='/service')?'border-slate-600 text-slate-600':'border-[#dfdede] text-[#555555]'}  hover:border-slate-600 hover:text-slate-600 hover:shadow-sm hover:shadow-slate-600`}>
            <span className={` hidden sm:flex`}>Services</span>
          </button>
          </Link>
          <Link smooth to='/#gallery'>
          <button className={`flex items-center pr-3 pl-1 mr-4 border-r-[3px] rounded-[4px] ${(location.pathname=='/gallery')?'border-slate-600 text-slate-600':'border-[#dfdede] text-[#555555]'}  hover:border-slate-600 hover:text-slate-600 hover:shadow-sm hover:shadow-slate-600`}>
            <span className={` hidden sm:flex`}>Gallery</span>
          </button>
          </Link>
          <Link smooth to='/#contact-us'>
          <button className={`flex items-center pr-3 pl-1 mr-4 border-r-[3px] rounded-[4px] ${(location.pathname=='/gallery')?'border-slate-600 text-slate-600':'border-[#dfdede] text-[#555555]'}  hover:border-slate-600 hover:text-slate-600 hover:shadow-sm hover:shadow-slate-600`}>
            <span className={` hidden sm:flex`}>Contact Us</span>
          </button>
          </Link>
        </div>
        :<></>}

        {data?(
          <div className="flex">
            <div>
              <Link to='/logout'>
                <button>Logout</button>
              </Link>
            </div>
          </div>
        ):(
          <div className="flex">
          <div className=''>
            <Link to="/login">
                <button className='px-4 py-1 mr-2 border-2 border-slate-600 shadow-[#ffffff] shadow-sm hover:shadow-md hover:shadow-[#ffffff] rounded-2xl text-[#232323]'>LOGIN</button>
            </Link>
          </div>
          <div className=''>
            <Link to="/register">
                <button className='px-4 py-1 border-2 border-slate-600 bg-slate-600 shadow-[#ffffff] shadow-sm hover:shadow-md hover:shadow-[#ffffff] rounded-2xl text-white'>BOOK AN APPOINTMENT</button>
            </Link>
          </div>
        </div>  
        )}
    </div>
</div>
  )
  }
export default Navbar